How to read the csv files titles:

<Data saved>_<parameter evolving>_<number of nodes>_<graph type>_<P[query Oracle]>_<batch number for Rosalind>.csv/xlsx

<data saved>
	- ET = Errors and Time
	- Val = Values of every node over time
	- Par = Key parameters of the network (degree, criticality etc...)
	- Pot = The evolution of the potential over time

<parameter evolving>
	- RN = number of redundant nodes
	- FN = number of faulty nodes

<graph type>
	- ER = Erdos Renyi
	- GRID = 2D square grid

<P[Query Oracle]>
	- 0 = probability depending on the number of faulty nodes
	- 1 = prob is 1, so always query oracle

<batch number>
	- number from 1 to 10. It means 10 batches of nIterations each