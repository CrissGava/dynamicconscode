# Simulate the problem setting for a dynamic average consensus. To run the script type
# python main_OverRedundantNodes.py <nIterations> <GRAPH_CHOICE> <pqo>
# Note that PQO = 0 for variable p_qo determined as in generate_graph. For PQO = q we have p_qo = 1.

import itertools
import math
import sys
import time
from multiprocessing import Pool

import networkx as nx
import numpy as np
import random
import statistics
from functools import partial
import pandas as pd
import sympy
import xlsxwriter

# <editor-fold desc="Flags">
ORACLE = True  # Flag to include the option of querying an oracle \Omega w.p. p_{qo}
DISPLAY = False
GRAPH_CHOICE = str(sys.argv[2])  # Type of graph tested
NODE_CHOICE = 3
PQO = int(sys.argv[3])
BATCH_N = int(sys.argv[4])  # batch number for splitting running jobs on Rosalind
# </editor-fold>

# <editor-fold desc="Define numerical quantities">
nIterations = int(sys.argv[1])  # Number of repetition for each value and graph generation
topVal = 100  # sampling interval [0, 100)
n = 64  # number of nodes
p_edgeExist = \
    7 * math.log(n) / n  # probability of an edge to exist. To assure the graph be connected, it needs to be >> log(n)/n
mu = 0  # Mean of the normal distribution
sigma = 1  # Standard deviation of the normal distribution

graph = nx.Graph()

faultyNodes = int(n / 10)  # int(math.sqrt(n))  # Number of faulty nodes in the graph
maxRedundantNodes = faultyNodes  # int(math.sqrt(n))  # number of redundant nodes from the oracle # TODO reset value
redundancy = range(maxRedundantNodes)
p_f = faultyNodes / n  # Probability for a node to be faulty


# </editor-fold>


# Makes the variable probabilityQueryOracle as global fro multiprocessing tasks
def init_pool(the_int):
    global probabilityQueryOracle
    probabilityQueryOracle = the_int


# Generates the graphs we will be using
def generateGraph(GRAPH_CHOICE, mP_f):
    if GRAPH_CHOICE == 'ER':
        graph = nx.erdos_renyi_graph(n, p_edgeExist, seed=None, directed=False)
    else:
        graph = nx.grid_graph((int(math.sqrt(n)), int(math.sqrt(n))))  # 2D square grid graph

    '''Graph properties'''
    degreesAll = [graph.degree()[k] for k in list(graph)]
    degrees = set(degreesAll)
    degreeDistribution = {k: degreesAll.count(k) for k in degrees}
    nodeDeg = np.mean(np.array(degreesAll))
    ac = nx.algebraic_connectivity(graph)  # The algebraic connectivity of the graph
    bc = nx.betweenness_centrality(graph)
    bc = np.array(list(bc.values()))  # node betweenness centrality
    criticalityVector = bc / np.array(degreesAll)
    networkCriticality = np.mean(criticalityVector)
    print(f'The criticality of the {GRAPH_CHOICE} graph is: {round(networkCriticality, 4)}' + '\n' +
          f'The algebraic connectivity is: {round(ac, 4)},' + '\n' +
          f'The betweenness centrality is: {round(np.mean(bc), 4)}' + '\n' +
          f'The average node degree is: {round(nodeDeg, 4)}')

    # Calculates the probability of querying an oracle. Note that this is constant given the graph type and depends
    # on the number of faulty nodes
    pqo = sum([(1 - (1 - mP_f * k / n) ** (9 * k / 10)) * (degreeDistribution[k] / n) for k in degrees])
    # print(f'P_qo = {pqo}')
    return pqo, graph, (round(networkCriticality, 4), round(ac, 4), round(np.mean(bc), 4), round(nodeDeg, 4))


# Returns the values of the neighbors of node <node>. If the ORACLE flag is true, returns also a set of mRed
# redundant values
def getNeighbors(mG, node, nNeighbors, mRed, mVals, p_qo):
    vals = [mVals[i] for i in nNeighbors]
    vals.append(mVals[node])

    if ORACLE:  # add to the vector other m values from non-neighboring nodes
        s = np.random.binomial(1, p_qo)
        if s:
            seq = list(mG)
            [seq.remove(i) for i in nNeighbors + [node]]
            redundancyNodes = random.sample(seq, mRed)
            redundancyValues = [mVals[i] for i in redundancyNodes]
            vals += redundancyValues

    return vals


# Get the errors wrt the initial mean and median
def getErrors(mMu, mNu, med):
    epsilonMu = abs(med - mMu)
    epsilonNu = abs(med - mNu)
    return epsilonMu, epsilonNu


# Calculates the potential as \phi = 1/2 \sum_{x, y}(\xi_x - \xi_y)^2
def calculatePotential(vals):
    sumVals = 0
    mValues = np.array([i for i in vals.values()])
    for i in mValues:
        for j in mValues:
            sumVals += (i - j) ** 2
    pot = 0.5 * sumVals
    return pot


# Calculates the potential as u = 1/2 \sum_{x \in V}\sum_{y in N_x}(\xi_y - \xi_x) = - 1/2 \Nabla \phi
def calculateNeighborsPotential(vals, A):
    pot = 0
    mValues = np.array([i for i in vals.values()])
    endIndex = A.shape[0]
    indices = range(endIndex)
    for i in indices:
        for j in range(i + 1, endIndex):
            if A[i, j]:
                pot += (mValues[j] - mValues[i])

    return pot


# Updates the value of each node to the median of its neighbors + itself
def updateValues(mG, v):
    temp = {}
    for j in list(mG):
        temp[j] = v[j]
    nx.set_node_attributes(mG, temp, 'Values')
    return mG


# Main function
def runLoops(G, red):
    m = red
    AdjMatrix = nx.adjacency_matrix(G)  # The adjacency matrix

    # Draw samples: for the working nodes, draw samples from a normal (0, 1) distribution. For the faulty nodes,
    # draw samples from a normal (\mu, \sigma) distribution where \mu is big
    workingNodesVals = np.random.normal(mu, sigma, size=n - faultyNodes)
    faultyNodesVals = np.random.normal(30, 3.5, size=faultyNodes)
    nodesVals = np.concatenate((workingNodesVals, faultyNodesVals), axis=0)
    np.random.shuffle(nodesVals)
    S = {list(G)[i]: nodesVals[i] for i in range(n)}
    nx.set_node_attributes(G, S, 'Values')

    t = 0
    delta = 10  # one-step potential change, initialization
    nu = np.mean(workingNodesVals)
    mValues = nx.get_node_attributes(G, 'Values')
    phi = calculatePotential(mValues)  # TODO uncomment these parts
    # neighborsPot = calculateNeighborsPotential(mValues, AdjMatrix)
    valuesEvolution = [list(
        mValues.values())]  # This will become an n x ? array. ? Because that value will be given by the final # of iterations taken in the while loop
    potentials = [phi]
    # potentials = [neighborsPot]
    M = False  # Final median

    deadlock = 0
    # while abs(neighborsPot) > 0.001 and deadlock < 300:
    while delta > 0.005 and deadlock < 300:
        deadlock += 1
        mediansDict = {}
        for j in list(G):
            neighs = list(G.adj[j])
            if PQO != 1:
                neighVals = getNeighbors(G, j, neighs, m, mValues, probabilityQueryOracle)
            else:  # For the case where probabilityQueryOracle = 100% we have
                neighVals = getNeighbors(G, j, neighs, m, mValues, PQO)
            mediansDict[j] = statistics.median(neighVals)

        # Update the graph and the potential function
        GNextStep = updateValues(G, mediansDict)
        phiNextStep = calculatePotential(mediansDict)  # TODO uncomment these parts
        # neighborsPot_NextStep = calculateNeighborsPotential(mediansDict, AdjMatrix)
        delta = abs(phiNextStep - phi)
        # delta = abs(neighborsPot_NextStep - neighborsPot)
        G = GNextStep
        mValues = mediansDict
        phi = phiNextStep
        # neighborsPot = neighborsPot_NextStep
        potentials.append(phi)
        # potentials.append(neighborsPot)
        valuesEvolution.append(list(mediansDict.values()))
        t += 1
        # print(f'Time T: {t}')
        # print(f'BEGINNING OF TIME {t}')
        # print(f'The potential change delta: {delta}')
        # print(f'The potential: {phi}')
        # print(f'The neighbors potential: {neighborsPot}')
        # print(f'The global median: {M}')

    print(f'Total Timesteps: {t}')

    # Once the algorithm converges, find the final median over all the nodes of the network
    M = statistics.median(np.array(list(mValues.values())))

    # Find the error estimates wrt M
    epsilonMu, epsilonNu = getErrors(mu, nu, M)

    # Prepare the array of the values evolution
    valuesEvolution = np.transpose(np.array(valuesEvolution))

    return red, epsilonMu, epsilonNu, t, potentials, valuesEvolution


if __name__ == '__main__':
    print('Starting...')
    start = time.time()
    # Prepare data structures to hold the results
    a = pd.DataFrame(columns=['epsilonMu', 'epsilonNu', 'T'])
    sumVals = np.zeros((len(redundancy), 3))
    mVals = np.zeros(len(redundancy))
    errorsAndTime = np.zeros((len(redundancy), 3))
    temp = []
    tempVals = {}

    # Iterate nIterations time and take the average of them in the end
    for i in range(nIterations):
        print(f'Iteration # {i}')
        probabilityQueryOracle, graph, spectralParams = generateGraph(GRAPH_CHOICE, p_f)
        P = len(redundancy)
        p = Pool(P, initializer=init_pool, initargs=(probabilityQueryOracle,))
        mapfunc = partial(runLoops, graph)
        res = p.map(mapfunc, redundancy)
        # Obtain the different values of m
        mVals += np.array([res[i][0] for i in redundancy])
        # Obtain the different values for epsilon_* and T
        errorsAndTime += np.array([res[i][1:4] for i in redundancy])
        # Obtain the vectors of the evolutions of phi
        temp.append([res[i][4] for i in redundancy])
        tempVals[i] = [res[i][5] for i in
                       redundancy]  # This is a dictionary of nIterations lists of redundancy (n x ?) matrices.

        # Save the parameters for subsequent plots
        if i == 0 and BATCH_N == 1:
            cols = np.array(['Use Oracle', 'Graph Choice', 'p_qo', 'n', 'p_f', 'Faulty Nodes',
                             'Criticality', 'Algebraic Conn', 'Betweenness Centrality', 'Node Deg'])
            prm = np.array(
                [ORACLE, GRAPH_CHOICE, round(probabilityQueryOracle, 4), n, round(p_edgeExist, 4), faultyNodes] + list(spectralParams))
            data = np.transpose(np.stack((cols, prm)))
            params = pd.DataFrame(data, columns=['Quantity', 'Value'])

    # To average all the different potential vectors, we first need to pad all the lists so to have uniform lenght
    # arrays. After that we can average among all the nIterations iterations and padd the final m values
    potentialsArray = {}
    pad_token = 0
    for j in redundancy:
        mList = [temp[i][j] for i in range(nIterations)]
        padded = zip(*itertools.zip_longest(*mList, fillvalue=pad_token))
        padded = np.array(list(padded))
        potentialsArray[j] = np.mean(padded, axis=0)

    potentialsDF = list(potentialsArray.values())
    potentialsDF = zip(*itertools.zip_longest(*potentialsDF, fillvalue=pad_token))
    potentialsDF = np.transpose(np.array(list(potentialsDF)))
    potentialsDF = pd.DataFrame(potentialsDF, columns=[f'Redundancy {i}' for i in redundancy])

    '''Wrt to the different values'''
    # writer = pd.ExcelWriter(f'./Results/ValuesOfNodes_overRedundantNodes_{n}Nodes_{GRAPH_CHOICE}_pqo_{PQO}_batch{BATCH_N}.xlsx',
    #                         engine='xlsxwriter')
    writer = pd.ExcelWriter(
        f'./Results/Val_RN_{n}Nodes_{GRAPH_CHOICE}_pqo_{PQO}_b{BATCH_N}.xlsx',
        engine='xlsxwriter')

    for r in redundancy:
        mList = [tempVals[i][r] for i in range(nIterations)]  # List of nIterations (n x ?) matrices
        mList2 = [[mList[i][j] for i in range(nIterations)] for j in range(n)]
        mList2 = [np.mean(np.array(list(zip(*itertools.zip_longest(*mList2[i], fillvalue=pad_token)))),
                          axis=0) for i in range(
            n)]  # List of n arrays with the same length. Each array is the mean of the evolution over a node
        mList2 = np.stack(mList2, axis=0)  # numpy  (n x max time) matrix
        mList2 = np.transpose(mList2)
        mDf = pd.DataFrame(mList2, columns=[f'{i}' for i in range(n)])
        mDf.to_excel(writer, index=False, sheet_name=f'Sheet{r + 1}')

    writer.save()

    results = errorsAndTime / nIterations
    mVals = mVals / nIterations

    dataToSave = pd.DataFrame(results, columns=['epsilonMu', 'epsilonNu', 'T'])
    dataToSave['# faulty nodes'] = mVals

    # dataToSave.to_csv(f'./Results/ErrorsAndTime_overRedundantNodes_{n}Nodes_{GRAPH_CHOICE}_pqo_{PQO}_batch{BATCH_N}.csv', index=False)
    dataToSave.to_csv(
        f'./Results/ET_RN_{n}Nodes_{GRAPH_CHOICE}_pqo_{PQO}_b{BATCH_N}.csv', index=False)
    if not PQO and BATCH_N == 1:
        # params.to_csv(f'./Results/Parameters_overRedundantNodes_{n}Nodes_{GRAPH_CHOICE}_pqo_0.csv', index=False)
        params.to_csv(f'./Results/Par_RN_{n}Nodes_{GRAPH_CHOICE}_pqo_0.csv', index=False)
    # potentialsDF.to_csv(f'./Results/PotentialsEvolution_{n}Nodes_{GRAPH_CHOICE}_pqo_{PQO}_batch{BATCH_N}.csv', index=False)
    potentialsDF.to_csv(f'./Results/Pot_RN_{n}Nodes_{GRAPH_CHOICE}_pqo_{PQO}_b{BATCH_N}.csv',
                        index=False)
    end = time.time()
    print(f'Total computation time: {end - start} seconds')
