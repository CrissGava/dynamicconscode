import matplotlib.pyplot as plt
import pandas as pd

n = 484
REDUNDANT = 0
GRAPH_CHOICE = 1
ORACLE = 1

if REDUNDANT:
    if not GRAPH_CHOICE:  # ER graph
        run1 = pd.read_csv('Results/ErrorsAndTime_overRedundantNodes_484Nodes_ER1.csv')
        run2 = pd.read_csv('Results/ErrorsAndTime_overRedundantNodes_484Nodes_ER2.csv')
        run3 = pd.read_csv('Results/ErrorsAndTime_overRedundantNodes_484Nodes_ER3.csv')
        run4 = pd.read_csv('Results/ErrorsAndTime_overRedundantNodes_484Nodes_ER4.csv')
        run5 = pd.read_csv('Results/ErrorsAndTime_overRedundantNodes_484Nodes_ER5.csv')
        fileName = 'Results/ResultsOverRedundantER.csv'
    else:
        run1 = pd.read_csv('Results/ErrorsAndTime_overRedundantNodes_484Nodes_GRID1.csv')
        run2 = pd.read_csv('Results/ErrorsAndTime_overRedundantNodes_484Nodes_GRID2.csv')
        run3 = pd.read_csv('Results/ErrorsAndTime_overRedundantNodes_484Nodes_GRID3.csv')
        run4 = pd.read_csv('Results/ErrorsAndTime_overRedundantNodes_484Nodes_GRID4.csv')
        run5 = pd.read_csv('Results/ErrorsAndTime_overRedundantNodes_484Nodes_GRID5.csv')
        fileName = 'Results/ResultsOverRedundantGRID.csv'
    cols = ['epsilonMu', 'epsilonNu', 'T', '# faulty nodes']
    results = pd.DataFrame(columns=cols)
    for c in cols:
        results[c] = (run1[c] + run2[c] + run3[c] + run4[c] + run5[c])/5
        # results[c] = (run1[c] + run3[c] + run4[c] + run5[c]) / 4

    results.to_csv(fileName)

    faultyNodes = int(n / 10)  # int(math.sqrt(n))  # Number of faulty nodes in the graph
    p_f = faultyNodes / n  # Probability for a node to be faulty

    fig, (ax1, ax2) = plt.subplots(2, 1)

    ax1.set_xlabel(r'$m$')
    ax1.set_ylabel(r'$\varepsilon_*$')
    ax1.set_title(r'Errors $\varepsilon_*$ as a function of $m$', pad=6)
    ax1.scatter(results['# faulty nodes'], [i for i in results['epsilonMu']], label=r'$\varepsilon_{\mu} = |\widetilde{\xi}(T) - \mu|$')
    ax1.scatter(results['# faulty nodes'], [i for i in results['epsilonNu']], label=r'$\varepsilon_{\nu} = |\widetilde{\xi}(T) - \nu|$')
    ax1.legend(loc='upper right')

    ax2.set_xlabel(r'$m$')
    ax2.set_ylabel(r'$T$')
    ax2.set_title(f'Time taken $T$ as a function of $m$')
    ax2.scatter(results['# faulty nodes'], [i for i in results['T']])
    if GRAPH_CHOICE == 0:
        textstring = r'$p_f=$ ' + str(round(p_f, 3)) + r' Faulty nodes$ =$ ' + str(faultyNodes) + '\nG is non-regular ' \
                                                                                                  'Erdos Renyi '
    else:
        textstring = r'$p_f=$ ' + str(round(p_f, 3)) + r' Faulty nodes$ =$ ' + str(faultyNodes) + '\nG is non-regular ' \
                                                                                                  'Square grid '

    # ax2.text(0.55, 0.95, textstring, transform=ax2.transAxes, fontsize=10,
    #          verticalalignment='top')

    # fig.suptitle(textstring)

    fig.tight_layout()
    if GRAPH_CHOICE == 0:
        fig.savefig(f'Results/ErrorsAndTime_overRedundantNodes_{n}Nodes_ER.png')
    else:
        fig.savefig(f'Results/ErrorsAndTime_overRedundantNodes_{n}Nodes_GRID.png')
else:  # Over faulty nodes
    if not GRAPH_CHOICE:  # ER graph
        run1 = pd.read_csv(f'Results/ErrorsAndTime_overFaultyNodes_484Nodes_ER1_Oracle{bool(ORACLE)}.csv')
        run2 = pd.read_csv(f'Results/ErrorsAndTime_overFaultyNodes_484Nodes_ER2_Oracle{bool(ORACLE)}.csv')
        run3 = pd.read_csv(f'Results/ErrorsAndTime_overFaultyNodes_484Nodes_ER3_Oracle{bool(ORACLE)}.csv')
        run4 = pd.read_csv(f'Results/ErrorsAndTime_overFaultyNodes_484Nodes_ER4_Oracle{bool(ORACLE)}.csv')
        run5 = pd.read_csv(f'Results/ErrorsAndTime_overFaultyNodes_484Nodes_ER5_Oracle{bool(ORACLE)}.csv')
        fileName = f'Results/ResultsOverFaultyER_Oracle{bool(ORACLE)}.csv'
    else:  # GRID graph
        run1 = pd.read_csv(f'Results/ErrorsAndTime_overFaultyNodes_484Nodes_GRID1_Oracle{bool(ORACLE)}.csv')
        run2 = pd.read_csv(f'Results/ErrorsAndTime_overFaultyNodes_484Nodes_GRID2_Oracle{bool(ORACLE)}.csv')
        run3 = pd.read_csv(f'Results/ErrorsAndTime_overFaultyNodes_484Nodes_GRID3_Oracle{bool(ORACLE)}.csv')
        run4 = pd.read_csv(f'Results/ErrorsAndTime_overFaultyNodes_484Nodes_GRID4_Oracle{bool(ORACLE)}.csv')
        run5 = pd.read_csv(f'Results/ErrorsAndTime_overFaultyNodes_484Nodes_GRID5_Oracle{bool(ORACLE)}.csv')
        fileName = f'Results/ResultsOverFaultyGRID_Oracle{bool(ORACLE)}.csv'

    cols = ['epsilonMu', 'epsilonNu', 'T', '# faulty nodes']
    results = pd.DataFrame(columns=cols)
    for c in cols:
        results[c] = (run1[c] + run2[c] + run3[c] + run4[c] + run5[c])/5

    results.to_csv(fileName)

    faultyNodes = int(n / 10)

    # results = pd.read_csv(fileName)
    # Plot the results
    fig, (ax1, ax2) = plt.subplots(2, 1)

    ax1.set_xlabel(r'$|F|$')
    ax1.set_ylabel(r'$\varepsilon_*$')
    ax1.set_title(r'Errors $\varepsilon_*$ as a function of $|F|$', pad=6)
    ax1.scatter(results['# faulty nodes'], [i for i in results['epsilonMu']], label=r'$\varepsilon_{\mu} = |\widetilde{\xi}(T) - \mu|$')
    ax1.scatter(results['# faulty nodes'], [i for i in results['epsilonNu']], label=r'$\varepsilon_{\nu} = |\widetilde{\xi}(T) - \nu|$')
    ax1.legend(loc='upper right')

    ax2.set_xlabel(r'$|F|$')
    ax2.set_ylabel(r'$T$')
    ax2.set_title(f'Time taken $T$ as a function of $|F|$')
    ax2.scatter(results['# faulty nodes'], [i for i in results['T']])
    if GRAPH_CHOICE == 0:
        textstring = r' Faulty nodes$ =$ ' + str(faultyNodes) + '\nG is non-regular Erdos Renyi '
    else:
        textstring = r' Faulty nodes$ =$ ' + str(faultyNodes) + '\nG is non-regular Square grid '

    ax2.text(0.55, -0.2, textstring, transform=ax2.transAxes, fontsize=10,
             verticalalignment='top')

    # plt.figtext(0.5, 0.1, textstring, ha='center', va='top', fontsize=10)

    # fig.suptitle(textstring)

    fig.tight_layout()
    if GRAPH_CHOICE == 0:
        fig.savefig(f'Results/ErrorsAndTime_overFaultyNodes_{n}Nodes_ER{bool(ORACLE)}.png')
    else:
        fig.savefig(f'Results/ErrorsAndTime_overFaultyNodes_{n}Nodes_GRID{bool(ORACLE)}.png')