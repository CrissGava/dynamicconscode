import sys

import matplotlib.pyplot as plt
import pandas as pd
import openpyxl

# TODO keep it in mind in case it needs changing
NODE_CHOICE = 3
n = 64
GRAPH_CHOICE = 'GRID'
params = pd.read_csv(f'./Results/Parameters_overRedundantNodes_{n}Nodes_{GRAPH_CHOICE}_pqo_0.csv')

# inputData0 = pd.read_csv(f'./Results/ErrorsAndTime_overRedundantNodes_{n}Nodes_{GRAPH_CHOICE}_pqo_0.csv')
inputData1 = pd.read_csv(f'./Results/ErrorsAndTime_overRedundantNodes_{n}Nodes_{GRAPH_CHOICE}_pqo_1.csv')

# potentialsEvolution0 = pd.read_csv(f'./Results/PotentialsEvolution_{n}Nodes_{GRAPH_CHOICE}_pqo_0.csv')
potentialsEvolution1 = pd.read_csv(f'./Results/PotentialsEvolution_{n}Nodes_{GRAPH_CHOICE}_pqo_1.csv')

mVals = [int(i) for i in inputData1['# faulty nodes']]
nodesEvolution1 = {}
for r in mVals:
    nodesEvolution1[r] = pd.read_excel(f'./Results/valuesOfNodes_overRedundantNodes_{n}Nodes_{GRAPH_CHOICE}_pqo_1.xlsx',
                                       sheet_name=f'Sheet{int(r + 1)}')


# Plot the results
plt.xlabel(r'$T$')
plt.ylabel(r'$\xi_i(t)$')
plt.title(r'Evolution of nodes values $\xi_i(t)$ as a function of $T$' + '\n' + r'$p_{qo} = 1$', pad=6)
[plt.plot(range(len(nodesEvolution1[3][:][f'{i}'])), nodesEvolution1[3][:][f'{i}']) for i in range(n)]
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.yscale('log')

# plt.text(0.5, 0, textstring, transform=ax2.transAxes, fontsize=10,
#          verticalalignment='top')

plt.tight_layout()
plt.show()
plt.savefig(f'./Results/valuesOfNodes_overRedundantNodes_{n}nodes_node{NODE_CHOICE}Nodes_{GRAPH_CHOICE}.png')

fig, (ax1, ax2) = plt.subplots(2, 1)

if GRAPH_CHOICE == 'ER':
    textstring = r'$p_f=$ ' + str(round(params['p_f'][0], 3)) + \
                 r' - Faulty nodes$ =$ ' + str(params['Faulty Nodes'][0]) + '\nG is non-regular Erdos Renyi\n'
else:
    textstring = r'Faulty nodes$ =$ ' + str(params['Faulty Nodes'][0]) + '\nG is non-regular Square grid\n'

ax1.set_xlabel(r'$m$')
ax1.set_ylabel(r'$\varepsilon_*$')
ax1.set_title(textstring + '\n' + r'Errors $\varepsilon_*$ as a function of $m$', pad=6)
# ax1.scatter(mVals, inputData0['epsilonMu'], label=r'$\varepsilon_{\mu}$, $p_{qo} =$' + str(round(params['p_qo'][0], 3)))
# ax1.scatter(mVals, inputData0['epsilonNu'], label=r'$\varepsilon_{\nu}$, $p_{qo} =$' + str(round(params['p_qo'][0], 3)))
ax1.scatter(mVals, inputData1['epsilonMu'], label=r'$\varepsilon_{\mu}$, $p_{qo} = 1$', marker='^')
ax1.scatter(mVals, inputData1['epsilonNu'], label=r'$\varepsilon_{\nu}$, $p_{qo} = 1$', marker='^')
ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))

ax2.set_xlabel(r'$m$')
ax2.set_ylabel(r'$T$')
ax2.set_title(f'Time taken $T$ as a function of $m$')
# ax2.scatter(mVals, inputData0['T'], label='$T$ for $p_{qo}$ ' + str(round(params['p_qo'][0], 3)))
ax2.scatter(mVals, inputData1['T'], label='$T$ for $p_{qo} = 1$')
ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5))

# ax2.text(0.5, 0.0, textstring, transform=ax2.transAxes, fontsize=10,
#          verticalalignment='top')

fig.tight_layout()
fig.savefig(f'./Results/ErrorsAndTime_overRedundantNodes_{n}Nodes_{GRAPH_CHOICE}.png')
plt.close(fig)

# Plot the values of a node
maxRedundantNodes = int(n / 10)  # int(math.sqrt(n))  # number of redundant nodes from the oracle # TODO reset value
redundancy = range(maxRedundantNodes)
# nodeVal0 = [potentialsEvolution0[f'Redundancy {i}'] for i in redundancy]
nodeVal1 = [potentialsEvolution1[f'Redundancy {i}'] for i in redundancy]

if params['Graph Choice'][0] == 'ER':
    textstring = r'$p_f=$ ' + str(round(params['p_f'][0], 3)) + \
                 r' - Faulty nodes$ =$ ' + str(params['Faulty Nodes'][0]) + '\nG is non-regular Erdos Renyi\n'
else:
    textstring = r'Faulty nodes$ =$ ' + str(params['Faulty Nodes'][0]) + '\nG is non-regular Square grid\n'

# fig2, (plt1, plt2) = plt.subplots(2, 1)
# plt1.set_xlabel(r'$T$')
# plt1.set_ylabel(r'$\phi(t)$')
# plt1.set_title(textstring + '\n' + r'Potential $\phi(t)$ as a function of $T$' + '\n'
#                + r'$p_{qo} = $' + str(round(params['p_qo'][0], 3)), pad=6)
# # [plt1.plot(range(len(nodeVal0[i])), nodeVal0[i], label=r'$m = $' + str(i)) for i in redundancy]
# plt1.legend(loc='center left', bbox_to_anchor=(1, 0.5))

# plt2.set_xlabel(r'$T$')
# plt2.set_ylabel(r'$\phi(t)$')
# plt2.set_title(r'Potential $\phi(t)$ as a function of $T$' + '\n' + r'$p_{qo} = 1$', pad=6)
# [plt2.plot(range(len(nodeVal1[i])), nodeVal1[i], label=r'$m = $' + str(i)) for i in redundancy]
# plt2.legend(loc='center left', bbox_to_anchor=(1, 0.5))

plt.xlabel(r'$T$')
plt.ylabel(r'$\phi(t)$')
plt.title(r'Potential $\phi(t)$ as a function of $T$' + '\n' + r'$p_{qo} = 1$', pad=6)
[plt.plot(range(len(nodeVal1[i])), nodeVal1[i], label=r'$m = $' + str(i)) for i in redundancy]
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.yscale('log')

# plt.text(0.5, 0, textstring, transform=ax2.transAxes, fontsize=10,
#          verticalalignment='top')

plt.tight_layout()
# plt.show()
plt.savefig(f'./Results/NodeEvolution_{n}nodes_node{NODE_CHOICE}Nodes_{GRAPH_CHOICE}.png')
