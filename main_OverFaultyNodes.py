# Simulate the problem setting for a dynamic average consensus
import math
import sys
import time
from multiprocessing import Pool

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import random
import statistics
from functools import partial
import pandas as pd

# random.seed(21)

'''Flags'''
ORACLE = True  # Flag to include the option of querying an oracle \Omega w.p. p_{qo}
DISPLAY = False
GRAPH_CHOICE = int(sys.argv[2])  # Type of graph tested

'''Define numerical quantities'''
nIterations = int(sys.argv[1])  # Number of repetition for each value and graph generation
topVal = 100  # sampling interval [0, 100)
n = 484   # number of nodes
p_edgeExist = \
    7 * math.log(n) / n  # probability of an edge to exist. To assure the graph be connected, it needs to be >> log(n)/n
print(f'P[edge exists]: {p_edgeExist}')
mu = 0  # Mean of the normal distribution
sigma = 1  # Standard deviation of the normal distribution

graph = nx.Graph()

pqo = {}
maxFaultyNodes = int(n / 10)
redundancy = range(maxFaultyNodes)
redundantNodes = int(maxFaultyNodes / 20)
p_f = [j / n for j in redundancy]

print(f'p_f = {p_f}')


def init_pool(the_int):
    global probabilityQueryOracle
    probabilityQueryOracle = the_int


def generateGraph(GRAPH_CHOICE, mP_f):
    '''Params to be changed depending on the simulation'''
    if GRAPH_CHOICE == 0:
        graph = nx.erdos_renyi_graph(n, p_edgeExist, seed=None, directed=False)
    else:
        graph = nx.grid_graph((int(math.sqrt(n)), int(math.sqrt(n))))  # 2D square grid graph

    '''Graph properties'''
    degreesAll = [graph.degree()[k] for k in list(graph)]
    degrees = set(degreesAll)
    degreeDistribution = {k: degreesAll.count(k) for k in degrees}

    for j in range(len(redundancy)):
        pqo[redundancy[j]] = sum(
            [(1 - (1 - mP_f[j] * k / n) ** (9 * k / 10)) * (degreeDistribution[k] / n) for k in degrees])
    print(f'P_qo = {pqo}')
    print(f'Degree distribution: {degreeDistribution}')
    print(f'Avg Degree distribution: {np.mean(np.array(degreesAll))}')
        # probabilityQueryOracle[redundancy[j]] = statistics.mean([1 - (1 - p_f[j] * k / n) ** (9 * k / 10) for k in degrees])

    return pqo, graph


def getNeighbors(mG, node, nNeighbors, mRed, mVals, p_qo):
    # vals = {}
    # vals = nx.get_node_attributes(mG, 'Values')
    # for i in list(mG):
    #     vals[i] = float('nan')
    #     if i in nNeighbors:
    #         vals[i] = nx.get_node_attributes(mG, 'Values')[i]
    # vals[node] = nx.get_node_attributes(mG, 'Values')[node]  # Remove this if self value not considered

    vals = [mVals[i] for i in nNeighbors]
    vals.append(mVals[node])

    if ORACLE:  # add to the vector other m values from non-neighboring nodes
        # p_qo = probabilityQueryOracle[mRed]
        s = np.random.binomial(1, p_qo[mRed])
        if s:
            seq = list(mG)
            [seq.remove(i) for i in nNeighbors + [node]]
            redundancyNodes = random.sample(seq, redundantNodes)
            redundancyValues = [mVals[i] for i in redundancyNodes]
            vals += redundancyValues
            # for j in redundancyNodes:
            #     vals[j] = nx.get_node_attributes(mG, 'Values')[j]

    return vals


# Get the errors wrt the initial mean and median
def getErrors(mMu, mNu, med):
    epsilonMu = abs(med - mMu)
    epsilonNu = abs(med - mNu)
    return epsilonMu, epsilonNu


# Calculates the potential as \phi = 1/2 \sum_{x, y}(\xi_x - \xi_y)^2
def calculatePotential(vals):
    sumVals = 0
    # for i in list(mG):
    #     for j in list(mG):
    #         sumVals += (mG.nodes[i]['Values'] - mG.nodes[j]['Values']) ** 2
    mValues = np.array(list(vals.values()))
    for i in mValues:
        for j in mValues:
            sumVals += (i - j) ** 2
    # sumVals = [(vals[i] - vals[j]) ** 2 for i, j in list(mG)]
    # sumVals = sum(sumVals)
    pot = 0.5 * sumVals
    return pot


# Get the median of the vector \xi_{x_{neigh}}
def getMedian(v):
    # temp = []
    # for i in list(v.values()):
    #     if not math.isnan(i):
    #         temp.append(i)
    # v = np.array(temp)
    # v = np.array(v)
    # v = np.sort(v)
    return statistics.median(v)


# Update the value of each node to the variance of it neighbors + itself
def updateValues(mG, v):
    temp = {}
    for j in list(mG):
        temp[j] = v[j]
    nx.set_node_attributes(mG, temp, 'Values')
    return mG


# Algorithm
def runLoops(G, red):
    nFaultyNodes = red

    # Draw samples: for the working nodes, draw samples from a normal (0, 1) distribution. For the faulty nodes,
    # draw samples from a normal (\mu, \sigma) distribution where \mu is big
    workingNodesVals = np.random.normal(mu, sigma, size=n - nFaultyNodes)
    faultyNodesVals = np.random.normal(30, 3.5, size=nFaultyNodes)
    nodesVals = np.concatenate((workingNodesVals, faultyNodesVals), axis=0)
    np.random.shuffle(nodesVals)
    S = {list(G)[i]: nodesVals[i] for i in range(n)}
    nx.set_node_attributes(G, S, 'Values')

    t = 0
    delta = 10  # one-step potential change, initialization
    potentials = []
    nu = np.mean(workingNodesVals)
    totalValues = []
    mValues = nx.get_node_attributes(G, 'Values')
    phi = calculatePotential(mValues)
    M = False  # Final median
    print(f'The potential at the beginning of time {t}: {phi}')

    while delta > 0.05:
        mediansDict = {}
        for j in list(G):
            neighs = list(G.adj[j])
            neighVals = getNeighbors(G, j, neighs, nFaultyNodes, mValues, probabilityQueryOracle)
            mediansDict[j] = getMedian(neighVals)

        mValues = nx.get_node_attributes(G, 'Values')
        GNextStep = updateValues(G, mediansDict)
        phiNextStep = calculatePotential(mValues)
        delta = abs(phiNextStep - phi)
        G = GNextStep
        phi = phiNextStep
        potentials.append(phi)
        for i in list(G):
            totalValues.append(nx.get_node_attributes(G, 'Values')[i])
        M = statistics.median(np.sort(np.array(totalValues)))
        t += 1
        print(f'BEGINNING OF TIME {t}')
        print(f'The potential change delta: {delta}')
        print(f'The potential: {phi}')
        print(f'The global median: {M}')

    epsilonMu, epsilonNu = getErrors(mu, nu, M)

    return red, (epsilonMu, epsilonNu, t - 1, potentials)


# def run():
if __name__ == '__main__':
    print('Starting...')
    start = time.time()
    a = pd.DataFrame(columns=['epsilonMu', 'epsilonNu', 'T'])
    sumVals = np.zeros((len(redundancy), 3))
    mVals = []
    for i in range(nIterations):
        print(f'Iteration # {i}')
        temp, graph = generateGraph(GRAPH_CHOICE, p_f)
        r = []
        P = len(redundancy)
        probabilityQueryOracle = temp
        p = Pool(P, initializer=init_pool, initargs=(probabilityQueryOracle,))
        mapfunc = partial(runLoops, graph)
        res = p.map(mapfunc, redundancy)
        r.append(res)
        mVals = [r[0][i][0] for i in redundancy]
        temp = [r[0][i][1] for i in redundancy]
        temp = np.array([[i[0], i[1], i[2]] for i in temp])
        sumVals += temp
    results = sumVals / nIterations
    data = np.array([[i[0], i[1], i[2]] for i in results])

    dataToSave = pd.DataFrame(data, columns=['epsilonMu', 'epsilonNu', 'T'])
    dataToSave['# faulty nodes'] = mVals

    # Plot the results
    fig, (ax1, ax2) = plt.subplots(2, 1)

    ax1.set_xlabel(r'$|F|$')
    ax1.set_ylabel(r'$\varepsilon_*$')
    ax1.set_title(r'Errors $\varepsilon_*$ as a function of $|F|$', pad=6)
    ax1.scatter(mVals, [i[0] for i in results], label=r'$\varepsilon_{\mu} = |\widetilde{\xi}(T) - \mu|$')
    ax1.scatter(mVals, [i[1] for i in results], label=r'$\varepsilon_{\nu} = |\widetilde{\xi}(T) - \nu|$')
    ax1.legend(loc='upper right')

    ax2.set_xlabel(r'$|F|$')
    ax2.set_ylabel(r'$T$')
    ax2.set_title(f'Time taken $T$ as a function of $|F|$')
    ax2.scatter(mVals, [i[2] for i in results])
    if GRAPH_CHOICE == 0:
        textstring = r' $m =$ ' + str(round(redundantNodes, 3)) + \
                     '\nG is non-regular Erdos Renyi ' + '\nOracle = ' + str(
            ORACLE)  # r'$p_f=$ ' + str([round(i, 3) for i in p_f]) +\
    else:
        textstring = r' $m =$ ' + str(round(redundantNodes, 3)) + \
                     '\nG is non-regular Square grid ' + '\nOracle = ' + str(
            ORACLE)  # r'$p_f=$ ' + str([round(i, 3) for i in p_f]) +\
    ax2.text(0.05, 0.95, textstring, transform=ax2.transAxes, fontsize=10,
             verticalalignment='top')

    fig.tight_layout()
    if GRAPH_CHOICE == 0:
        fig.savefig(f'ErrorsAndTime_overFaultyNodes_{n}Nodes_ER_Oracle{ORACLE}.png')
        dataToSave.to_csv(f'ErrorsAndTime_overFaultyNodes_{n}Nodes_ER{sys.argv[3]}_Oracle{ORACLE}.csv', index=False)
    else:
        fig.savefig(f'ErrorsAndTime_overFaultyNodes_{n}Nodes_GRID_Oracle{ORACLE}.png')
        dataToSave.to_csv(f'ErrorsAndTime_overFaultyNodes_{n}Nodes_GRID{sys.argv[3]}_Oracle{ORACLE}.csv', index=False)

    end = time.time()
    print(f'Total computation time: {end - start} seconds')