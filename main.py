# Simulate the problem setting for a dynamic average consensus
import math
from multiprocessing import Pool

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import random
import statistics
from functools import partial

random.seed(21)

'''Flags'''
ORACLE = True  # Flag to include the option of querying an oracle \Omega w.p. p_{qo}
DISPLAY = False
GRAPH_CHOICE = 1  # Type of graph tested
SIMULATION_TYPE = 'ErrorsAndTimeOver_m'
# SIMULATION_TYPE = 'ErrorsAndTimeOver_FaultyNodes'

'''Define numerical quantities'''
topVal = 100  # sampling interval [0, 100)
n = 49  # number of nodes
p_edgeExist = \
    7 * math.log(n) / n  # probability of an edge to exist. To assure the graph be connected, it needs to be >> log(n)/n
mu = 0  # Mean of the normal distribution
sigma = 1  # Standard deviation of the normal distribution
# if SIMULATION_TYPE == 0:
#     probabilityQueryOracle = 0
# else:
#     probabilityQueryOracle = {}

# if SIMULATION_TYPE == 'ErrorsAndTimeOver_m':
#     maxRedundantNodes = int(math.sqrt(n))  # number of redundant nodes from the oracle
#     nFaultyNodes = int(math.sqrt(n))  # Number of faulty nodes in the graph
#     p_f = nFaultyNodes / n  # Probability for a node to be faulty
#     probabilityQueryOracle = sum(
#         [k / n * p_f * degreeDistribution[k] for k in degrees])  # E[Pr(having a faulty neighbor)]
# else:
#     maxFaultyNodes = int(n / 10)
#     redundantNodes = int(maxFaultyNodes / 20)
#     p_f = maxFaultyNodes / n
#     # probabilityQueryOracle = sum(
#     #     [k / n * p_f * degreeDistribution[k] for k in degrees])  # E[Pr(having a faulty neighbor)]

# if GRAPH_CHOICE == 0:
#     degreeDistribution = {k: math.comb(n, k) * p_edgeExist ** k * (1 - p_edgeExist) ** (n - 1 - k) for k in degrees}
# else:
#     degreeDistribution = {k: degreesAll.count(k) for k in degrees}

# Display the graph
# if DISPLAY:
#     nx.draw(graph)
#     plt.show()


# S = {i: topVal * random.random() for i in range(n)}

# # Calculate the probability of querying an oracle \Omega
# def getProbQo(k):
#     return 1 - (1 - (math.log(k)) / (n ** 1.5)) ** math.sqrt(k)


# Get the values of the neighbors of 'node' and return them in an array on n elements
# where the non-neighbors are set to nan

class InputPar:
    def __init__(self, myGraph, myP_qo):
        self.graph = myGraph
        self.pqo = myP_qo


def getNeighbors(mG, node, nNeighbors, mRed, probabilityQueryOracle):
    vals = {}  # np.zeros(n)
    for i in list(mG):
        vals[i] = float('nan')
        if i in nNeighbors:
            vals[i] = nx.get_node_attributes(mG, 'Values')[i]
    vals[node] = nx.get_node_attributes(mG, 'Values')[node]  # Remove this if self value not considered

    if ORACLE:  # add to the vector other m values from non-neighboring nodes
        if SIMULATION_TYPE == 'ErrorsAndTimeOver_m':
            p_qo = probabilityQueryOracle
        else:
            p_qo = probabilityQueryOracle[mRed]
        s = np.random.binomial(1, p_qo)
        if s:
            seq = list(mG)
            [seq.remove(i) for i in nNeighbors + [node]]
            redundancyNodes = random.sample(seq, mRed)
            for j in redundancyNodes:
                vals[j] = nx.get_node_attributes(mG, 'Values')[j]

    return vals


# Get the errors wrt the initial mean and median
def getErrors(mMu, mNu, med):
    epsilonMu = abs(med - mMu)
    epsilonNu = abs(med - mNu)
    return epsilonMu, epsilonNu


# Calculates the potential as \phi = 1/2 \sum_{x, y}(\xi_x - \xi_y)^2
def calculatePotential(mG):
    sumVals = 0
    for i in list(mG):
        for j in list(mG):
            sumVals += (mG.nodes[i]['Values'] - mG.nodes[j]['Values']) ** 2
    pot = 0.5 * sumVals
    return pot


# Get the median of the vector \xi_{x_{neigh}}
def getMedian(v):
    temp = []
    for i in list(v.values()):
        if not math.isnan(i):
            temp.append(i)
    v = np.array(temp)
    v = np.sort(v)
    return statistics.median(v)


# Update the value of each node to the variance of it neighbors + itself
def updateValues(mG, v):
    temp = {}
    for j in list(mG):
        temp[j] = v[j]
    nx.set_node_attributes(mG, temp, 'Values')
    return mG


# Algorithm
def runLoops(red, par):
    # TODO: set m to red whenever you want to vary the number of redundant nodes. To nFaultyNodes whenever you want
    # to vary the number of faulty nodes.
    G = par.graph
    if SIMULATION_TYPE == 'ErrorsAndTimeOver_m':
        m = red
        nFaultyNodes = int(math.sqrt(n))
    else:
        m = redundantNodes
        nFaultyNodes = red

    # Draw samples: for the working nodes, draw samples from a normal (0, 1) distribution. For the faulty nodes,
    # draw samples from a normal (\mu, \sigma) distribution where \mu is big
    workingNodesVals = np.random.normal(mu, sigma, size=n - nFaultyNodes)
    faultyNodesVals = np.random.normal(30, 3.5, size=nFaultyNodes)
    nodesVals = np.concatenate((workingNodesVals, faultyNodesVals), axis=0)
    np.random.shuffle(nodesVals)
    S = {list(G)[i]: nodesVals[i] for i in range(n)}
    # plt.hist(list(S.values()), bins=50)
    # plt.show()
    nx.set_node_attributes(G, S, 'Values')

    t = 0
    delta = 10  # one-step potential change, initialization
    potentials = []
    nu = np.mean(workingNodesVals)
    totalValues = np.zeros(n)
    phi = calculatePotential(G)
    M = False  # Final median
    print(f'The potential at the beginning of time {t}: {phi}')

    while delta > 0.05:
        arrayOfMedians = np.ones(n) * (topVal + 10)
        for j in list(G):
            neighs = [i for i in G.neighbors(j)]
            neighVals = getNeighbors(G, j, neighs, m, par.pqo)
            arrayOfMedians[j] = getMedian(neighVals)

        GNextStep = updateValues(G, arrayOfMedians)
        phiNextStep = calculatePotential(GNextStep)
        delta = abs(phiNextStep - phi)
        G = GNextStep
        phi = phiNextStep
        potentials.append(phi)
        for i in list(G):
            totalValues[i] = nx.get_node_attributes(G, 'Values')[i]
        M = statistics.median(np.sort(totalValues))
        t += 1
        print(f'BEGINNING OF TIME {t}')
        print(f'The potential change delta: {delta}')
        print(f'The potential: {phi}')
        print(f'The global median: {M}')

    epsilonMu, epsilonNu = getErrors(mu, nu, M)

    return par.red, (epsilonMu, epsilonNu, t - 1)


# def run():
if __name__ == '__main__':
    '''Params to be changed depending on the simulation'''
    if GRAPH_CHOICE == 0:
        graph = nx.erdos_renyi_graph(n, p_edgeExist, seed=None, directed=False)
    else:
        graph = nx.grid_graph((int(math.sqrt(n)), int(math.sqrt(n))))  # 2D square grid graph

    '''Graph properties'''
    degreesAll = [graph.degree()[k] for k in list(graph)]
    degrees = set(degreesAll)
    meanDeg = sum(degreesAll) / len(degreesAll)
    degreeDistribution = {k: degreesAll.count(k) for k in degrees}

    global probabilityQueryOracle
    if SIMULATION_TYPE == 'ErrorsAndTimeOver_m':
        maxRedundantNodes = int(math.sqrt(n))  # number of redundant nodes from the oracle
        redundancy = range(maxRedundantNodes)
        faultyNodes = int(math.sqrt(n))  # Number of faulty nodes in the graph
        p_f = faultyNodes / n  # Probability for a node to be faulty
        probabilityQueryOracle = sum(
            [k / n * p_f * degreeDistribution[k] for k in degrees])
    else:
        maxFaultyNodes = int(n / 10)
        redundancy = range(maxFaultyNodes)
        redundantNodes = int(maxFaultyNodes / 20)
        p_f = [j / n for j in redundancy]
        for j in range(len(redundancy)):
            probabilityQueryOracle[redundancy[j]] = sum(
                [k / n * p_f[j] * degreeDistribution[k] for k in degrees])  # E[Pr(having a faulty neighbor)]

    inputVar = InputPar(graph, probabilityQueryOracle)
    r = []
    P = len(redundancy)
    p = Pool(P)
    mapfunc = partial(runLoops, inputVar)
    r.append(p.map(mapfunc, redundancy))
    # r.append(p.map(runLoops, inputVar))
    mVals = [r[0][i][0] for i in redundancy]
    results = [r[0][i][1] for i in redundancy]

    # Plot the results
    fig, (ax1, ax2) = plt.subplots(2, 1)

    ax1.set_xlabel(r'$|F|$')
    ax1.set_ylabel(r'$\varepsilon_*$')
    ax1.set_title(r'Errors $\varepsilon_*$ over $|F|$', pad=6)
    ax1.scatter(mVals, [i[0] for i in results], label=r'$\varepsilon_{\mu} = |\widetilde{\xi}(T) - \mu|$')
    ax1.scatter(mVals, [i[1] for i in results], label=r'$\varepsilon_{\nu} = |\widetilde{\xi}(T) - \nu|$')
    ax1.legend(loc='upper right')

    ax2.set_xlabel(r'$|F|$')
    ax2.set_ylabel(r'$T$')
    ax2.set_title(f'Time taken $T$ over $|F|$')
    ax2.scatter(mVals, [i[2] for i in results])
    if GRAPH_CHOICE == 0:
        textstring = r'$p_f=$ ' + str(p_f) + r' $m =$ ' + str(meanDeg) + r'\n$G$ is non-regular Erdos Renyi'
    else:
        textstring = r'$p_f=$ ' + str(p_f) + r' $m =$ ' + str(meanDeg) + r'\n$G$ is non-regular Square grid'
    ax2.text(0.05, 0.95, textstring, transform=ax2.transAxes, fontsize=10,
             verticalalignment='top')

    fig.tight_layout()
    # plt.show()
    # fig.savefig(f'ErrorsAndTime_overM_{n}Nodes.png')
    if GRAPH_CHOICE == 0:
        fig.savefig(f'ErrorsAndTime_overFaultyNodes_{n}Nodes_ER.png')
    else:
        fig.savefig(f'ErrorsAndTime_overFaultyNodes_{n}Nodes_GRID.png')
    # for pValue in outpt:
    #     ssf.write_to_excel(nCountries, agentsPerCountry, pValue, path, betaString)

    # if __name__ == '__main__':
    #     run()
